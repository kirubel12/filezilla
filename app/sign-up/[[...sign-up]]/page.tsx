import React from "react";
import { SignUp } from "@clerk/nextjs";
const RegisterPage = () => {
  return (
    <div className="flex justify-center">
      <SignUp redirectUrl="/sign-in" />
    </div>
  );
};

export default RegisterPage;
