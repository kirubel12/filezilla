"use client";
import HeroSection from "@/components/component/Hero-section";

const Home = () => {
  return (
    <main>
      <HeroSection />
    </main>
  );
};

export default Home;
