"use client";
import Sidebar from "@/components/component/Sidebar";
import { storage } from "@/utils/firebase";
import { useState } from "react";
import { TrashIcon } from "@heroicons/react/24/outline";
import prettyBytes from "pretty-bytes";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

type FileProps = {
  name?: string;
  size?: number;
  type?: string;
};
const UploadPage = () => {
  const [file, setFile] = useState<File>();

  const uploadFile = async ({ size, name }: FileProps) => {
    if (size && size > 10240000) {
      toast.error("File is too big  ", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
      });
    } else if (name) {
      toast.success("Upload Successfully  ", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
      });
    }
  };

  return (
    <>
      <div className="flex justify-center items-center sm:gap-4 md:gap-72">
        <div className="h-screen sm:max-w-[300px] md:max-w-[500px] sm:m-4 mt-12 justify-center w-full">
          <label
            htmlFor="dropzone-file"
            className="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-white dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-white hover:border-indigo-500 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600"
          >
            <div className="flex flex-col items-center justify-center pt-5 pb-6">
              <svg
                className="w-12 h- mb-4 text-indigo-800 dark:text-gray-400"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 16"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"
                />
              </svg>
              <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                <span className="font-bold text-indigo-800">
                  Click to upload
                </span>{" "}
                or drag and drop
              </p>
              <p className="text-xs text-gray-500 dark:text-gray-400">
                PNG,JPG max 10MB
              </p>
            </div>
            <input
              id="dropzone-file"
              onChange={(e) => setFile(e.target.files?.[0])}
              type="file"
              className="hidden"
            />
          </label>

          {file ? (
            <div className="bg-white rounded-lg shadow-md p-4 mt-4 text-wrap">
              <div className="flex items-center justify-between">
                <h2 className="text-lg font-medium text-gray-900">
                  {file?.name}
                </h2>
                <TrashIcon
                  className="w-6 h-6 cursor-pointer"
                  onClick={() => setFile(undefined)}
                />
              </div>
              <div className="mt-2">
                <p className="text-sm text-gray-500">
                  {prettyBytes(file?.size)}
                </p>
                <p className="text-sm text-gray-500">{file?.type}</p>
              </div>
            </div>
          ) : null}
          <div className="flex justify-center mt-4">
            <button
              disabled={!file}
              onClick={() => uploadFile({ size: file?.size, name: file?.name })}
              className="px-8 py-2 disabled:bg-gray-300 text-white bg-indigo-600 mt-2 rounded-md"
            >
              Upload
            </button>
          </div>
        </div>
      </div>
      <ToastContainer limit={1} />
    </>
  );
};

export default UploadPage;
