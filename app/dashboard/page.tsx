import Sidebar from "@/components/component/Sidebar";
import React from "react";

const Dashboard = () => {
  return (
    <div className="flex gap-x-4">
      <Sidebar />
      <div className="p-4">
        <h2 className="text-2xl font-bold">Dashboard</h2>
      </div>
    </div>
  );
};

export default Dashboard;
