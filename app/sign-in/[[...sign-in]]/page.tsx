import { SignIn } from "@clerk/nextjs";
const LoginPage = () => {
  return (
    <div className="flex justify-center mt-2">
      <SignIn afterSignInUrl="/dashboard" />
    </div>
  );
};

export default LoginPage;
