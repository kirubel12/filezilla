import React from "react";
import { Button } from "../ui/button";

const HeroSection = () => {
  return (
    <section className="bg-gray-50">
      <div className="mx-auto max-w-screen-xl px-4 py-12 lg:flex lg:h-screen">
        <div className="mx-auto max-w-xl text-center">
          <h1 className="text-3xl font-extrabold sm:text-5xl leading-relaxed">
            Share Files Instantly & Securely with
            <strong className="font-extrabold text-indigo-700 sm:block mt-2">
              Filezilla
            </strong>
          </h1>

          <p className="mt-4 sm:text-xl/relaxed">
            Streamline workflows, collaborate seamlessly, and forget the
            attachment headaches.
          </p>

          <div className="mt-8 flex flex-wrap justify-center gap-4">
            <Button className="px-6 py-2">Get Started</Button>

            <Button variant="outline" className="px-6 py-2">
              Learn More
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeroSection;
