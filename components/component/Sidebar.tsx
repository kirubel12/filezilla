import Link from "next/link";
const Sidebar = () => {
  return (
    <div className="flex h-screen flex-col justify-between border-e bg-white">
      <div className="px-6 py-6">
        <ul className=" space-y-1">
          <li>
            <Link
              href=""
              className="block rounded-lg px-4 py-2 text-sm font-medium text-gray-500 hover:bg-indigo-400 hover:text-white"
            >
              Upload
            </Link>
          </li>

          <li>
            <Link
              href=""
              className="block rounded-lg px-4 py-2 text-sm font-medium text-gray-500 hover:bg-indigo-400 hover:text-white"
            >
              My Files
            </Link>
          </li>

          <li>
            <Link
              href=""
              className="block rounded-lg px-4 py-2 text-sm font-medium text-gray-500 hover:bg-indigo-400 hover:text-white"
            >
              Favorites
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
